function carModelSort(invent) {
    if (invent) {
        invent.sort(function (a, b) {
            const aModel = a["car_model"]
            const bModel = b["car_model"]
            if (aModel > bModel) {
                return 1
            }
            if (aModel < bModel) {
                return -1
            }
            return 0
        })

        return invent
    }
    return "Inventory doesn't exist"
}

module.exports = carModelSort