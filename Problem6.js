function BMW_AUDI(invent) {
    let B_A = []
    if (invent) {
        for (let i = 0; i < invent.length; i++) {
            if (invent[i].car_make === 'BMW' || invent[i].car_make === 'Audi')
                B_A.push(invent[i])
        }
    }
    return B_A
}

module.exports = BMW_AUDI