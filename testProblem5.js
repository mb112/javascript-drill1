const carYears = require('./Problem4.js')
const inventory = require('./cars(js-drill1).js')
const oldCars = require('./Problem5')




console.log('Test case 1 =', oldCars(carYears,inventory ))
console.log('Test case 2 =', oldCars(carYears, []))
console.log('Test case 3 =', oldCars(carYears, null))
console.log('Test case 4 =', oldCars(null, null))
console.log('Test case 5 =', oldCars(function empty(){}, inventory))
console.log('Test case 6 =', oldCars(function empty(){}, null))
console.log('Test case 7 =', oldCars(null, inventory))