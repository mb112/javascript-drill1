function lastCar(cars) {
    if (cars) {
        if (cars.length === 0) {
            return "No car in Lot"
        } else {
            return `Last car is a ${cars[cars.length - 1]['car_make']} ${cars[cars.length - 1]['car_model']}`
        }

    }
    return "Inventory doesn't exist"
}

module.exports = lastCar