

function carYears(invent) {
    if (invent){
        const carYear = []

        for (let i = 0; i < invent.length; i++) {
            carYear.push(invent[i]['car_year'])
        }

        return carYear
    }
    return "Inventory doesn't exist"
}

module.exports = carYears

