function carById(cars, id) {
    if (cars) {
        for (let i = 0; i < cars.length; i++) {
            if (cars[i]['id'] === id) {
                return `Car ${cars[i].id} is a ${cars[i].car_year} ${cars[i].car_make} ${cars[i].car_model}`
            }

        }
        return 'No such car in lot'
    }
    return "Inventory doesn't exist"
}

module.exports = carById;



