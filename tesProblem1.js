const carById = require('./Problem1.js')
const inventory = require('./cars(js-drill1).js')


console.log('Test case 1 =', carById(inventory, 33))
console.log('Test case 2 =', carById(inventory, 13))
console.log('Test case 3 =', carById(inventory, 330))
console.log('Test case 4 =', carById(null, null))
console.log('Test case 5 =', carById(inventory, null))